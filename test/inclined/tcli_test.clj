(ns inclined.tcli-test
  (:require
   [clojure.test :refer [deftest testing is are]]
   [clojure.tools.cli :as clj-cli]
   [inclined.tcli :as sut]
   [matcher-combinators.matchers :as m]
   [matcher-combinators.test]))


(deftest config-test
  (testing "global edition"
    (are [global-cfg args parsed-options-sub] (match? parsed-options-sub
                                                    (->> {:global global-cfg}
                                                         (sut/config)
                                                         :global
                                                         (clj-cli/parse-opts args)))
      ;; default added options
      ;; help
      {} '("-h") {:options {:help? true}}
      ;; help in summary
      {} '()     {:summary #"-h, --help +show this message"}

      ;; simple option...
      ;; in result when provided
      {:options [{:name "foo"}]} '("--foo" "bar") {:options {:foo "bar"}}

      ;; in summary
      ;; shows 'long'
      {:options [{:name "foo"}]}                        '() {:summary #"--foo FOO"}
      ;; shows 'short'
      {:options [{:name "foo" :short "-f"}]}            '() {:summary #"-f, --foo FOO"}
      ;; shows description when given
      {:options [{:name "foo" :desc "adds foo"}]}       '() {:summary #"--foo FOO +adds foo"}
      ;; override 'long'
      {:options [{:name "foo" :long "--foo SOME-FOO"}]} '() {:summary #"--foo SOME-FOO\n"}
      ;; no errors when absent
      {:options [{:name "foo"}]}                        '() {:errors nil}
      ;; TODO default values (+ shown in summary)

      ;; bang-options
      ;; error when not provided
      {:options [{:name "foo!"}]} '() {:errors ["Missing required option --foo"]}
      ;; TODO marked as required in summary
      ;; {:options [{:name "foo!"}]} '() {:summary #"--foo FOO +(required)"}

      ;; question-options
      ;; use correct keyword and value
      {:options [{:name "foo?"}]}                  '("--foo") {:options {:foo? true}}
      ;; default value when *not* provided
      {:options [{:name "foo?"}]}                  '()        {:options {:foo? false}}
      ;; default value when provided
      {:options [{:name "foo?" :default true}]}    '()        {:options {:foo? true}}
      ;; long
      {:options [{:name "foo?" :desc "add foo?"}]} '()        {:summary #"--\[no-\]foo +add foo?"}

      ;; int types
      {:options [{:name "foo" :type :int}]} '("--foo" "123") {:options {:foo 123}}
      ;; TODO parse error
      )))


(deftest determine-cli-action-global-test
  (testing "yields correct action"
    (are [parse-result result] (= result
                                  (sut/determine-cli-action-global parse-result
                                                                   {:commands #{"known-command"}}))
      ;; errors
      {:errors ["Some invalid flag \"-b\""]} :global/has-error

      ;; help
      {:options {:help? true}}            :global/help-requested
      {:options {:help? true} :errors []} :global/help-requested

      ;; help > errors, e.g. given a required flag `foo`, when parsing `-h`
      ;; yields an error that foo is missing: this should not prevent
      ;; the help from being shown.
      {:errors  ["Missing required options --foo"]
       :options {:help? true}}                          :global/help-requested

      ;; no command
      {:arguments []} :global/no-command

      ;; unknown command
      {:command :unknown-command} :global/unknown-command

      ;; no action needed
      {:command "known-command"} nil)))


(deftest parse-opts-test
  (testing "for subcommand 'foo' in commands-mode"
    (let [fut #(sut/parse-args % {:global   [["-h" "--help" "Global help" :id :help?]
                                             ["-v" "--version" "Global version"]]
                                  :mode     :commands
                                  :commands {"foo" [["-h" "--help" "foo help" :id :help?]]}})]
      (are [args expected] (match? expected (fut args))
        '("-h")  {:action :global/help-requested}
        '("bar") {:action :global/unknown-command}

        '("foo")      {:action :command/run}
        '("foo" "-V") {:action :command/has-error}
        '("foo" "-h") {:command-parsed {:options {:help? true}}})))

  (testing "when in main-mode"
    (let [fut #(sut/parse-args % {:mode     :main
                                  :commands {"-main" [["-h" "--help" "foo help" :id :help?]
                                                      ["-d" "--debug" nil :id :debug?]]}})]
      (are [args expected] (match? expected (fut args))
        '("-h")              {:action :command/help-requested}
        ;; command is always "-main"
        '("bar")             {:command "-main" :action :command/run}
        ;; all non-flags are left as arguments
        '("bar" "-d" "moar") {:command-parsed {:arguments ["bar" "moar"]
                                               :options   {:debug? true}}}))))
