(ns inclined.config-test
  (:require
   [clojure.test :refer [deftest is testing are]]
   [dummy-cli]
   [inclined.config :as sut]
   [matcher-combinators.matchers :as m]
   [matcher-combinators.test]))

(def extract-from-ns-meta #'sut/extract-from-ns-meta)
(def extract-from-fn #'sut/extract-from-fn)
(def extract-options #'sut/extract-options)

(deftest extract-from-ns-meta-test
  (testing "desc"
    (are [ns-meta expected] (match? expected (extract-from-ns-meta ns-meta))
      {:doc "some docs"} {:desc "some docs"}

      ;; inclined-desc takes precedence over doc
      {:doc "doc" :inclined/desc "some docs"} {:desc "some docs"}))

  (testing "options"
    (are [ns-meta expected] (match? expected (extract-from-ns-meta ns-meta))
      #:inclined {:option.verbose? {}} {:options [{:name "verbose?"}]}
      #:inclined {:option.a {}
                  :option.b {}} {:options [{:name "a"} {:name "b"}]})))


(deftest extract-options-test
  (are [m expected] (= expected (extract-options m))
    ;; simple
    #:inclined {}                     []
    #:inclined {:option.foo {}}       [{:name "foo"}]
    #:inclined {:option.with-dash {}} [{:name "with-dash"}]

    ;; contents of map preserved
    #:inclined {:option.a {:b 2}} [{:name "a" :b 2}]

    ;; multiple
    #:inclined {:option.a {}
                :option.b {}} [{:name "a"} {:name "b"}]

    ;; non-inclined kws ignored
    #:inclined {:_/option.foo {}} []))


(defn ^#:inclined{:option.foo {}
                  :desc "The real desc"}
  some-task
  "docstring"
  [])

(deftest extract-from-fn-test
  (is (match? {:desc "The real desc"} (extract-from-fn #'some-task)))
  (is (match? {:options [{:name "foo"}]} (extract-from-fn #'some-task)))
  (is (match? {:name "some-task"} (extract-from-fn #'some-task)))
  ;; match? does not support vars
  (is (= {:fn #'some-task} (-> #'some-task extract-from-fn (select-keys [:fn])))))


(deftest extract-test
  (testing "global"
    (let [fut #(:global (sut/extract (the-ns 'dummy-cli)))]
      (is (match? {:desc "global-desc"} (fut)))
      (is (match? {:options [{:name "verbose"}]} (fut)))))

  (testing "commands"
    (let [fut #(:commands (sut/extract (the-ns 'dummy-cli)))]
      (is (match? {"dummy-command" {:desc "command-desc"}} (fut)))
      (is (match? {"dummy-command" {:options [{:name "foo"}]}} (fut)))
      ;; only public functions are considered commands
      (is (= '("-main" "dummy-command") (keys (fut))))))

  (testing "mode"
    (let [fut #(:mode (sut/extract (the-ns 'dummy-cli)))]
      (is (= :main (fut))))))
