(ns test.inclined.action-test
  (:require [clojure.test :as t :refer [deftest testing is are]]
            [inclined.action :as sut]
            [matcher-combinators.matchers :as m]
            [matcher-combinators.test]))

(deftest help-commands-test
  (testing "for when :desc absent"
    (is (re-matches #" *cmd *"
                    (sut/help-commands-section {:commands [["cmd" {}]]}))))

  (testing "shows only first line of :desc"
    (is (re-matches #" *cmd *first line"
                    (sut/help-commands-section {:commands [["cmd" {:desc "first line\nsecond line"}]]})))))
