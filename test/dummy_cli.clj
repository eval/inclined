(ns dummy-cli
  "global-desc"
  #:inclined{:option.verbose {}})

(def ignore-me 1)

(defn ^#:inclined{:option.foo{}}
  dummy-command
  "command-desc"
  [])

(defn- ^#:inclined{:option.should-not-exist {}}
  no-command
  "this command should be ignored"
  [])

(defn -main
  "turns mode to :main"
  [])
