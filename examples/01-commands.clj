#!/usr/bin/env ../exe/inclined
"DEPS='https://gitlab.com/eval/inclined.git=18202f0c4aa69ca352ec5aaf5847047bc5004c8d;clj-time=RELEASE'"
"CLI_NS=commands-cli"

(ns commands-cli
  "CLI with several commands and an extra dependency"
  #:inclined{:option.debug? {:desc "global option for debugging"}}
  (:require
   [clj-time.core :as t]))


(defn ^#:inclined{:option.foo {:desc "some value for foo"}}
  command1
  "some description for command1"
  [{:keys [foo]}]
  (prn :command1 :foo foo))


(defn ^#:inclined{:option.bar {:desc "some value for bar"}}
  command2
  "does not much"
  [{:keys [bar global/debug?]}]
  (prn "Hello from command2!" :bar bar :debug? debug? :now (str (t/now))))
