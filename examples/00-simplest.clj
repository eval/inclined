#!/usr/bin/env ../exe/inclined
"DEPS='https://gitlab.com/eval/inclined.git=18202f0c4aa69ca352ec5aaf5847047bc5004c8d'"

(defn
  ^#:inclined{:option.foo  {:desc "add some foo"}
              :option.bar! {:desc "(required) in need of bar"}
              :option.baz? {:desc "to baz or not to baz..."}}
  -main
  "A simple standalone CLI"
  [{:keys [foo bar baz?]}]
  (prn :main :foo foo :bar bar :baz? baz?))
