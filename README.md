# inclined

Create powerful command-line interfaces in Clojure.

_ALPHA-ware!_ this project is under development and breaking changes *will* occur!

*repository:* https://gitlab.com/eval/inclined

[![pipeline status](https://gitlab.com/eval/inclined/badges/master/pipeline.svg)](https://gitlab.com/eval/inclined/commits/master)
[![discuss at Clojurians-Zulip](https://img.shields.io/badge/clojurians%20zulip-clojure-brightgreen.svg)](https://clojurians.zulipchat.com/#narrow/stream/151168-clojure)

## shortcut to happiness

Or: from 0 to CLI in under 20 seconds.

``` shell
# write the CLI
$ cat <<EOF> hello.clj
(ns hello-cli)

(defn ^#:inclined{:option.to {:desc "whom to greet"}}
  -main
  "says hello"
  [{:keys [to] :or {to "world"}}]
  (println (str "Hello, " to "!")))
EOF

# see the help
$ clj -Sdeps '{:deps {inclined {:git/url "https://gitlab.com/eval/inclined.git" :sha "18202f0c4aa69ca352ec5aaf5847047bc5004c8d"}}}' -i hello.clj -m inclined.main --ns hello-cli -- -h
says hello

USAGE
  $ cli [options]

OPTIONS
      --to TO  whom to greet
  -h, --help   show this message

# run it
$ clj -Sdeps '{:deps {inclined {:git/url "https://gitlab.com/eval/inclined.git" :sha "18202f0c4aa69ca352ec5aaf5847047bc5004c8d"}}}' -i hello.clj -m inclined.main --ns hello-cli -- --to 'fellow Clojurians'
Hello, fellow Clojurians!
```

For more see `examples/*.clj`.

## nrepl server

``` shell
$ clj -A:nREPL
# or full:
$ clj -Sdeps '{:deps {cider/cider-nrepl {:mvn/version "0.18.0"} }}' -e '(require (quote cider-nrepl.main)) (cider-nrepl.main/init ["cider.nrepl/cider-middleware"])'
```
