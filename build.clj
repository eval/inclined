(ns user
  "Some really simple CLI"
  #:inclined{:option.verbose? {}})

(defn ^#:inclined{:option.foo {}}
  hello
  "will send a greeting"
  [& args]
  (prn :hello :args args))

(ns ns-with-main)

(defn ^#:inclined{:option.to {:desc "to whom to send it"}}
  -main
  "will send a greeting"
  [{:keys [to cli/cmd-args]}]
  (prn :cli-args cmd-args)
  (println (str "sending this to " to)))


(ns feed-sync-cli
  "Some description"
  #:inclined{:desc "Some description of this cli"})


(defn ^#:inclined{:option.seen {:type :string :default "seen.edn"}}
  hn
  "Syncs hn-feed"
  [& args]
  (prn :hn :args args))


(defn ^#:inclined{:option.seen {:desc "what value was last seen"}}
  reddit
  "Syncs reddit-feed"
  [{:keys [seen]}]
  (prn :reddit :seen seen))



(defn ^#:inclined{:option.to   {:desc      "(required) to whom this should be send"
                                :short     "-t"
                                :required? true}
                  :option.from {:desc "(default: 'anon') sender of the message"}}
  greet
  "Send greeting"
  [{:keys [to from] :or {from "anon"}}]
  (prn (str from "->" to ": 'niks'")))






(comment
  (ns inclined
    (:require
     [clojure.pprint :as pprint]
     [clojure.repl :refer [dir-fn]]
     [clojure.tools.cli :as cli]))


  (defn cli-ns-config
    "Yields collection of metadata from cli-namespace and all its functions."
    [cli-ns]
    (let [cli-fns     (dir-fn cli-ns)
          cli-ns-meta (meta cli-ns)
          fn->var     #(find-var (symbol (str cli-ns) (str %)))]
      (conj (map (comp meta fn->var) cli-fns)
            cli-ns-meta)))


  (defn find-cli-ns []
    (letfn [(cli-match [ns]
              (and (re-find #"-cli$" (str ns))
                   ns))]
      (some cli-match (all-ns))))

  (cli/parse-opts '("-b") [["-h" "--help" "Show this help"]] :in-order true)

  ;; - fouten:
  ;;   - onbekende global opt => err "unknown option ..."
  ;;   - onbekend command => "unknown command ..."
  ;;   - onbekende opties voor command => "unknown option ..."

  (cli-ns-config (find-cli-ns))
  (defn -main [& args]
    (let [nses (all-ns)]
      (prn nses))))
