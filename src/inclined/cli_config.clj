(ns inclined.cli-config
  (:require
   [clojure.string :as string]
   [clojure.tools.cli :as cli]
   [inclined.core]))


(defmulti process-name #(->> % :name (re-find #"[\?\!]$")))

(defmethod process-name "?" [{name :name :as option}]
  (let [canonical-name (-> name (string/split #"\?$") first)]
    (as-> option o
      (assoc o :name canonical-name)
      (assoc o :id   (keyword name))
      (assoc o :type :boolean)
      (if-not (:required? o)
        (assoc o :default false)
        o))))

(defmethod process-name "!" [{name :name :as option}]
  (let [canonical-name (-> name (string/split #"\!$") first)]
    (as-> option o
      (assoc o :name canonical-name)
      (assoc o :id   (keyword canonical-name))
      (assoc o :required? true)
      (merge {:type :string} o))))

(defmethod process-name :default [{name :name :as option}]
  (as-> option o
    (assoc o :id (keyword name))
    (merge {:type :string} o)))


(defmulti add-long :type)

(defmethod add-long :boolean [{name :name :as option}]
  (let [long (str "--[no-]" name)]
    (assoc option :long long)))

(defmethod add-long :default [{name :name :as option}]
  (let [long (str "--" name " " (string/upper-case name))]
    (assoc option :long long)))


(defmulti maybe-parse-fn :type)

(defmethod maybe-parse-fn :int [option]
  (merge {:parse-fn #(Integer/parseInt %)} option))

(defmethod maybe-parse-fn :default [option] option)


(defmulti maybe-missing :required?)

(defmethod maybe-missing true [{name :name :as option}]
  (merge {:missing (str "Missing required option --" name)} option))

(defmethod maybe-missing :default [option] option)


(defn convert-to-tools-cli-option [option]
  (-> []
      (conj (:short option))
      (conj (:long option))
      (conj (:desc option))
      (into cat (select-keys option [:id :missing :parse-fn :default :default-desc]))))


(defn ensure-convertable-option [option]
  (-> option
      (process-name)
      (add-long)
      (maybe-parse-fn)
      (maybe-missing)))


(defn option->tools-cli-option [option]
  (-> option
      (ensure-convertable-option)
      (convert-to-tools-cli-option)))
