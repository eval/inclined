(ns inclined.config
  (:require
   [clojure.repl :refer [dir-fn]]
   [clojure.string :as string]))


(defn- extract-options
  "Example:
  (extract-options {:inclined/option.foo {:other :keys}})
  => {:name \"foo\" :other :keys}"
  [m]
  (reduce (fn [acc [k v]]
            (if-let [option-name (and (= "inclined" (namespace k))
                                      (peek (re-find #"^option\.([^.]+)" (name k))))]
              (conj acc (assoc v :name option-name))
              acc)) [] m))


(defn- extract-from-fn
  "Example:
  (defn ^#:inclined{:option.seen? {}}
    greet
    \"greet someone\"
    [{:keys [seen?]}])

  (extract-from-fn #'my-command)
  ;; => {:desc \"greet someone\" :name \"greet\" :fn #'my-ns/greet
         :options [{:name \"seen?\"}]}
  "
  [fn-var]
  (let [fn-meta                          (meta fn-var)
        {:keys [doc inclined/desc name]} fn-meta
        options                          (extract-options fn-meta)]
    {:desc    (or desc doc)
     :name    (str name)
     :fn      fn-var
     :options options}))


(defn- extract-from-ns-meta
  "All config-data from `ns`"
  [ns-meta]
  (let [{doc :doc desc :inclined/desc} ns-meta
        options                        (extract-options ns-meta)]
    {:desc (or desc doc) :options options}))


(defn- determine-mode [{commands :commands :as _config}]
  (if (some #{"-main"} (keys commands))
    :main
    :commands))


(defn extract [ns]
  (let [sym->ns-var      #(find-var (symbol (str ns) (str %)))
        ns-vars          (map sym->ns-var (dir-fn ns))
        command?         (fn [var] (-> var meta (contains? :arglists)))
        commands         (map extract-from-fn (filter command? ns-vars))
        commands-by-name (reduce #(assoc %1 (:name %2) %2) {} commands)]
    (as-> {:global   (extract-from-ns-meta (meta ns))
           :commands commands-by-name} $
      (assoc $ :mode (determine-mode $)))))
