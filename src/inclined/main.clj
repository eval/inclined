(ns inclined.main
  (:require
   [inclined.action :refer [handle-action]]
   [inclined.cli]
   [inclined.config :as config]
   [inclined.tcli :as tcli]))


(defn -main [& args]
  (let [config (config/extract (the-ns 'inclined.cli))
        parsed (tcli/parse-args args (tcli/config config))]
    (handle-action parsed config)))
