(ns inclined.action
  (:require
   [clojure.pprint :as pp]
   [clojure.string :as string]))


(defn help-commands-section [{commands :commands :as _config}]
  (let [command->name-desc-tuple (fn [[cmd {desc :desc}]]
                                   (let [short-desc (-> desc str (string/split-lines) first)]
                                     [cmd short-desc]))
        name-desc-tuples         (map command->name-desc-tuple commands)
        max-cmd-name-width       (apply max (map (comp count first) name-desc-tuples))]
    (string/join \newline
                 (map #(pp/cl-format nil "~{  ~vA  ~A~}"
                                     (cons max-cmd-name-width %))  name-desc-tuples))))


(defn help-msg
  "Yields the global help message.
  Requires the result of tcli/parse and the global config"
  [{summ :summary :as _parsed} {{global-desc :desc} :global :as config}]
  (let [sections {:desc     global-desc
                  :options  summ
                  :usage    (str "$ cli [options] [COMMAND] [command-options]")
                  :commands (help-commands-section config)}]
    (-> []
        (conj (:desc sections))
        (conj (str "USAGE" \newline "  " (:usage sections)))
        (conj (str "OPTIONS" \newline (:options sections)))
        (conj (str "COMMANDS" \newline (:commands sections)))
        (as-> a
            (interpose nil a)
          (string/join \newline a)))))


(defn command-help-msg [{{summ :summary} :command-parsed
                         cmd             :command :as _parsed}
                        {commands :commands
                         mode     :mode}]
  (let [cmd-config (get commands cmd)
        sections   {:desc    (:desc cmd-config)
                    :usage   (case mode
                               :main     (str "$ cli [options]")
                               :commands (str "$ cli [global-options] " cmd " [options]"))
                    :options summ}]
    (as-> [] $
      (conj $ (:desc sections))
      (conj $ (str "USAGE" \newline "  " (:usage sections)))
      (conj $ (str "OPTIONS" \newline (:options sections)))
      (interpose nil $)
      (string/join \newline $))))


(defmulti *handle-action (fn [parsed _config] (:action parsed)))

;; global actions

(defmethod *handle-action :global/has-error [{[err] :errors} config]
  {:stderr (str "Error: " err ".") :exit 1})

(defmethod *handle-action :global/help-requested [parsed config]
  {:stdout (help-msg parsed config)})

(defmethod *handle-action :global/no-command [parsed config]
  {:stdout (help-msg parsed config)})

(defmethod *handle-action :global/unknown-command [{command :command} config]
  {:stderr (str "Error: unknown command '" command "'. See --help for commands.") :exit 1})


;;; command actions

(defmethod *handle-action :command/has-error [{{[err] :errors} :command-parsed} config]
  {:stderr (str "Error: " err ".") :exit 1})

(defmethod *handle-action :command/help-requested [parsed config]
  {:stdout (command-help-msg parsed config)})

(defmethod *handle-action :command/run [{global-options          :options
                                         cmd                      :command
                                         {cmd-options :options
                                          cmd-args    :arguments} :command-parsed}
                                        {commands :commands :as config}]
  (let [namespacify    (fn [ns kw] (keyword (name ns) (name kw)))
        global-options (reduce (fn [acc [k v]]
                                 (assoc acc (namespacify :global k) v))
                               {} global-options)
        options        (merge global-options cmd-options {:cli/cmd-args cmd-args})
        {cmd-fn :fn}   (get commands cmd)]
    (cmd-fn options)))


(defn handle-action [parsed config]
  (let [{:keys [stderr stdout exit] :or {exit 0}} (*handle-action parsed config)]
    (when stderr
      (println stderr))
    (when stdout
      (println stdout))
    (System/exit exit)))
