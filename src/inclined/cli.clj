(ns inclined.cli
  (:require
   [inclined.config :as config]
   [inclined.tcli :as tcli]
   [inclined.action :refer [handle-action]]))

(defn
  ^#:inclined{:option.ns     {:desc "namespace that contains cli-config"}
              :option.debug? {:desc  "output debugging"
                              :short "-d"}}
  -main
  "turn a simple clj into a CLI

USAGE

$ cli -i some-cli.clj -m inclined.main [options] -- [cli-options]

EXAMPLES

Given the file `some-cli.clj` with the following contents:
(ns some-cli
    \"some simple cli\")

(defn ^#:inclined{:option.to {:desc \"recipient of message\"}}
      greet
      \"send a greeting\"
      [{:keys [to]}]
      (println (str \"Hello, \" to \"!\")))

Use it:
$ clj -i some-cli.clj -m inclined.main --ns some-cli -- greet --to 'fellow clojurians'
Hello, fellow clojurians!

You get a generic help (`clj... -- -h`) and command-specific help (`clj... -- greet -h`) out of the box.

Add global options to the cli by adding metadata to the ns:
(ns some-cli
    \"some simple cli\"
    #:inclined{:option.debug? {:desc \"show debug information\"}})

The debug-option will be available in the greet-function as `global/debug?`:
(defn ^#:inclined{...}
      greet
      \"...\"
      [{:keys [to global/debug?]}] ...)
"
  [{:keys [ns debug? cli/cmd-args] :or {ns "user"}}]
  (let [cli-config (config/extract (-> ns symbol (doto (require)) (find-ns)))]
    (when debug?
      (prn :cli-config cli-config))
    (handle-action (tcli/parse-args cmd-args (tcli/config cli-config)) cli-config)))
