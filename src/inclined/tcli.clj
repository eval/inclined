(ns inclined.tcli
  (:require
   [clojure.string :as string]
   [clojure.tools.cli :as clj-cli]
   [inclined.config :as config]))


(defmulti process-name #(->> % :name (re-find #"[\?\!]$")))

(defmethod process-name "?" [{name :name :as option}]
  (let [canonical-name (-> name (string/split #"\?$") first)]
    (as-> option o
      (assoc o :name canonical-name)
      (assoc o :id   (keyword name))
      (assoc o :type :boolean)
      (if-not (:required? o)
        (assoc o :default (-> option :default boolean))
        o))))

(defmethod process-name "!" [{name :name :as option}]
  (let [canonical-name (-> name (string/split #"\!$") first)]
    (as-> option o
      (assoc o :name canonical-name)
      (assoc o :id   (keyword canonical-name))
      (assoc o :required? true)
      (merge {:type :string} o))))

(defmethod process-name :default [{name :name :as option}]
  (as-> option o
    (assoc o :id (keyword name))
    (merge {:type :string} o)))


(defmulti add-long :type)

(defmethod add-long :boolean [{name :name :as option}]
  (let [long (str "--[no-]" name)]
    (assoc option :long long)))

(defmethod add-long :default [{name :name provided-long :long :as option}]
  (let [long (or provided-long (str "--" name " " (string/upper-case name)))]
    (assoc option :long long)))


(defmulti maybe-parse-fn :type)

(defmethod maybe-parse-fn :int [option]
  (merge {:parse-fn #(Integer/parseInt %)} option))

(defmethod maybe-parse-fn :default [option] option)


(defmulti maybe-missing :required?)

(defmethod maybe-missing true [{name :name :as option}]
  (merge {:missing (str "Missing required option --" name)} option))

(defmethod maybe-missing :default [option] option)


(defn ensure-convertable-option [option]
  (-> option
      (process-name)
      (add-long)
      (maybe-parse-fn)
      (maybe-missing)))


(defn convert-option [option]
  (-> []
      (conj (:short option))
      (conj (:long option))
      (conj (:desc option))
      (into cat (select-keys option [:id :missing :parse-fn :default :default-desc]))))


(defn option->tcli-option [option]
  (-> option
      (ensure-convertable-option)
      (convert-option)))


(defn determine-cli-action-global
  "Given the parsed args and the args, determine what to do next.
  Phase in #{:global :command}
  "
  [{errors         :errors
    command        :command
    {help? :help?} :options}
   {:keys [commands]}]
  (cond
    help?                    :global/help-requested
    (seq errors)             :global/has-error
    (nil? command)           :global/no-command
    (not (commands command)) :global/unknown-command))


(defn determine-cli-action-command
  [{{errors        :errors
     {help? :help?} :options} :command-parsed}]
  (cond
    help?        :command/help-requested
    (seq errors) :command/has-error
    :else        :command/run))


(def help-option ["-h" "--help" "show this message" :id :help?])


(defn config [{{global-options :options} :global
               commands                  :commands
               mode                      :mode :as _global-config}]
  (let [tclify           #(mapv option->tcli-option %)
        add-help-flag    #(conj % help-option)
        global-options   (add-help-flag (tclify global-options))
        commands-options (into {}
                               (map (fn [[_ {options :options} :as cmd]]
                                      (assoc cmd 1 (add-help-flag (tclify options)))) commands))]
    {:global   global-options
     :mode     mode
     :commands commands-options}))


(defmulti parse-args (fn [_args {mode :mode :as _cli-config}] mode))

(defmethod parse-args :main [args {{main-cfg "-main"} :commands}]
  (as-> {:command "-main"} $
    (assoc $ :command-parsed (clj-cli/parse-opts args main-cfg))
    (assoc $ :action (determine-cli-action-command $))))

(defmethod parse-args :commands [args {global-cfg :global commands-cfg :commands mode :mode :as cli-config}]
  (let [commands                          (-> commands-cfg keys set)
        {[command & rem-args] :arguments :as parsed} (clj-cli/parse-opts args global-cfg :in-order true)
        parsed                            (assoc parsed :command command :arguments rem-args)]
    (if-let [action (determine-cli-action-global parsed {:commands commands})]
      (assoc parsed :action action)
      (as-> parsed $
        (assoc $ :command-parsed
               (clj-cli/parse-opts (:arguments parsed) (get commands-cfg command)))
        (assoc $ :action (determine-cli-action-command $))))))
